import pandas as pd
import string
import numpy as np


def readFile(filepath):
    df = pd.read_csv(filepath, delimiter=';')
    df = df.drop(columns=['Unnamed: 2', 'Unnamed: 3', 'Unnamed: 4'])
    return df


def toTxt(df):
    return ' '.join(list(df["SLOGAN"]))


def getChars(txt):
    print('total chars: ', len(sorted(list(set(txt)))))
    return sorted(list(set(txt)))


def createDicts(chars):
    char_indices = dict((c, i) for i, c in enumerate(chars))
    indices_char = dict((i, c) for i, c in enumerate(chars))
    return char_indices, indices_char


def getXY(text, maxlen, step, chars, char_indices):
    sentences = []
    next_chars = []
    for i in range(0, len(text) - maxlen, step):
        sentences.append(text[i: i + maxlen])
        next_chars.append(text[i + maxlen])
    x = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
    y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
    for i, sentence in enumerate(sentences):
        for t, char in enumerate(sentence):
            x[i, t, char_indices[char]] = 1
        y[i, char_indices[next_chars[i]]] = 1
    return x, y


df = readFile('inputData.csv')
print(df.head(5))

txt = toTxt(df)
chars = getChars(txt)
# print(txt)

char_indices, indices_char = createDicts(chars)

print(char_indices)
print(indices_char)

maxlen = 40
step = 3

x, y = getXY(txt, maxlen, step, chars, char_indices)
