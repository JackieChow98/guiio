import pandas as pd
from pathlib import Path
import numpy as np
import random

def fitness(sol, data):
    x = sol["x"]
    Dis = data.values
    N = len(x)

    Z = 0
    for k in range(N-1):
        i = x[k]
        j = x[k+1]
        Z = Z + Dis[i, j]
    sol["fit"] = Z
    return sol

#### ROULETTE WHEEL ####
def indices(a):
    for i in a:
        if i < random.uniform(0, 1):
            return i

def RouletteWheel(P):
    if sum(P) == 0:
        P = P + random.uniform(0, 1)
    P = P/sum(P)
    P = np.cumsum(P)
    return indices(P)
#########################

###### INSERT DATA ######
excelPath = Path.cwd() / 'data2.xlsx'
dis = pd.read_excel(excelPath, header=None).drop(0).drop(0, axis=1).replace(np.nan, 2e11)

M = 2e11
nvar = 20
n_ant = 100
maxiter = 5
Snode = 0
Fnode = 21
#########################

##### INITIALIZATION ####
tau0 = 1
tau = tau0 * np.ones((nvar, nvar))

diag1 = np.empty(nvar)
diag1.fill(1)
tau = tau - np.diag(diag1)

etha = 1/dis

diag2 = np.empty(len(etha))
diag2.fill(np.inf)
etha = etha - np.diag(diag2)
etha = pd.DataFrame(etha).fillna(0)

Q = 100
alpha = 8
beta = 10
Roh = 0.02

U = np.ones((nvar, nvar))
helper = 1
for i in range(nvar):
    for j in range(nvar):
        U[j][i] = helper
        helper += 1
############################

###### MAIN LOOP ######
emp = {}
emp["x"] = []
emp["fit"] = []
ant = []

for i in range(n_ant):
    ant.append(emp)

# gant = {}
# gant["x"] = []
# gant["fit"] = np.inf
BEST = np.zeros((maxiter,), dtype=int)
BEST = []
MEAN = np.zeros((maxiter,), dtype=int)

Route = [Snode-1, Fnode-1]
URoute = int(U[Snode-1, Fnode-1])
ant[0]["x"] = Route
ant[0] = fitness(ant[0], dis)
L = ant[0]["fit"]
helper2 = 0
for i in range(len(tau)):
    for j in range(len(tau)):
        if helper2 == URoute-1:
            tau[i, j] = tau[i, j] + Q / L
        helper2 = helper2 + 1

#tau[URoute-1] = tau[URoute-1] + Q/L
gant = ant[0]
for iter in range(maxiter - 1):

    for k in range(n_ant - 1):

        Route = [Snode]
        URoute = np.zeros((nvar,), dtype=int)
        for v in range(1, nvar - 1):
            i = Route[-1]
            P = (tau[i-1, :] ** alpha) * (etha.values[i-1, :] ** beta)
            P = [0 if elem in Route else elem for elem in P]
            j = int(RouletteWheel(P))
            Route.append(j)
            URoute[v-1] = U[i, j]
            print(URoute)

            if j == Fnode:
                break

        ant[k]["x"] = Route
        ant[k] = fitness(ant[k], dis)

        L = ant[k]["fit"]
        URoute = [elem for elem in URoute if elem != 0]
        #tau = [elem + Q/L if elem in URoute else elem for elem in tau]
        tau[list(URoute)] += Q/L

    tau = tau * (1 - Roh)

    value = np.inf
    index = -1
    for i in range(len(ant)):
        if ant[i]["fit"] < value:
            value = ant[i]["fit"]
            index = i

    if value < gant["fit"]:
        gant = ant(index)

    #BEST[iter] = gant["fit"]
    BEST.append(gant["fit"])

    print("Iteracja numer: ", iter + 1, " Najlepsza iteracja: ", BEST[iter])



